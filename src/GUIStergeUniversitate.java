import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class GUIStergeUniversitate extends JFrame {
	private JLabel label;
	private JTextField _ID;
	private JButton okbtn;
	public GUIStergeUniversitate() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("Sterge Universitate");
		frame.setLayout(new GridLayout(0,2));
		JPanel mainframe = new JPanel();
		mainframe.setLayout(new BorderLayout());
		okbtn = new JButton("OK");
		mainframe.add(okbtn,BorderLayout.EAST);
		_ID = new JTextField();
		label = new JLabel("ID");	
		frame.add(label);
		frame.add(_ID);
		frame.add(mainframe);
		frame.setSize(400,100);
		Handler handle = new Handler();
		okbtn.addActionListener(handle);
		mainframe.setVisible(true);
		frame.setVisible(true);
	}
	private class Handler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == okbtn) {
				if (!new Universitate().delete(_ID.getText())) {
					JOptionPane.showMessageDialog(null, "Universitatea cu ID " + _ID.getText()  + " nu a fost gasita");
				}
				else {
					JOptionPane.showMessageDialog(null, "Universitate stearsa");			
			}
			}
			
		}
		
	}

}
