import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
@SuppressWarnings("serial")
public class GUIModificaUniversitate extends JFrame{
	private JLabel l_tel;
	private JLabel l_adresa;
	private JTextField tel;
	private JTextField adresa;
	private JButton modbtn;
	private JList<String> lista_univ;
	public GUIModificaUniversitate() {
		setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame();
		modbtn = new JButton("Modifica Universitate");
		l_tel = new JLabel("Telefon");
		tel = new JTextField();
		l_adresa = new JLabel("Adesa");
		adresa = new JTextField();
		final String u[] = new Universitate().mod();
		lista_univ = new JList<String>(u);
		frame.add(lista_univ);
		frame.add(l_tel);
		frame.add(tel);
		frame.add(l_adresa);
		frame.add(adresa);
		frame.setLayout(new GridLayout(0,1));
		JPanel mainframe = new JPanel();
		mainframe.setLayout(new BorderLayout());
		mainframe.add(modbtn, BorderLayout.SOUTH);
		frame.add(mainframe);
		frame.setSize(400,400);
		frame.setLocationRelativeTo(null);
		frame.setTitle("Modifica Universitate");
		Handle handler = new Handle();
		modbtn.addActionListener(handler);
		frame.setVisible(true);
	}
	private class Handle implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == modbtn) {
				String s = (String) lista_univ.getSelectedValue();
				Universitate u = new Universitate().fs(s);
				u.setTel(tel.getText());
				u.setAdresa(adresa.getText());
				if (u.update(u.getID(),u.getDenumire(),u.getAdresa(),u.getTel())) {
				JOptionPane.showMessageDialog(null, "Universitate modificata");
				}
				else {
					JOptionPane.showMessageDialog(null, "Eroare");
				}
			}	
			}
		}
	}
