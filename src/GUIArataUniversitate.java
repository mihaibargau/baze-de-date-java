import javax.swing.JFrame;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class GUIArataUniversitate extends JFrame {
	private JTextArea textArea;
	public GUIArataUniversitate() {
		JFrame.setDefaultLookAndFeelDecorated(true);
		textArea = new JTextArea();
		JFrame frame = new JFrame("Arata Universitati");
		show();
		frame.add(textArea);
		textArea.setEditable(false);
		frame.pack();
		frame.setVisible(true);
	}
	public void show()
	{
		textArea.append(new Universitate().showAll());
	}

}
