import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Universitate {
	private String ID;
	private String denumire;
	private String adresa;
	private String tel;
	final static int MAX_SIZE = 1000;
	public Universitate(String denumire,String adresa,String tel)
	{
		this.setDenumire(denumire);
		this.setAdresa(adresa);
		this.setTel(tel);
	}
	public Universitate(String ID,String denumire,String adresa,String tel) {
		this.ID = ID;
		this.setDenumire(denumire);
		this.setAdresa(adresa);
		this.setTel(tel);
	}
	public Universitate(){
		this.denumire = "";
		this.adresa = "";
		this.tel = "";
	}
	public boolean save()
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	boolean action = false;
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "INSERT INTO Universitate (denumire, adresa, tel) VALUES ( " + "\'" + denumire
        			+ "\'" + ", " +  "\'" + adresa + "\'" + ", " + "\'" + tel + "\'" + " );";
        	PreparedStatement pstmt = con.prepareStatement(query);
        	action = (pstmt.executeUpdate() > 0);
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return action;
	}
	public boolean update(String ID,String denumire,String adresa,String tel)
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	boolean action = false;
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "UPDATE Universitate SET denumire= " + "\'" + denumire + "\'"
        			+ " ,adresa=" + "\'" + adresa + "\'" 
        			+ " ,tel=" +  "\'" + tel + "\'" + " WHERE ID=" + ID + ";";
        	PreparedStatement pstmt = con.prepareStatement(query);
        	action = (pstmt.executeUpdate() > 0);
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
		return action;
	}
	public String showAll()
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	ResultSet rs = null;
    	String line = "";
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "SELECT * FROM Universitate";
        	PreparedStatement pstmt = con.prepareStatement(query);
        	rs = pstmt.executeQuery();
        	while(rs.next())
        	{
        		line += rs.getString("ID");
        		line += " " + rs.getString("denumire");
        		line += " " + rs.getString("adresa");
        		line += " " + rs.getString("tel");
        		line += "\n";
        	}
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return line;
		
	}
	public String[] mod()
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	ResultSet rs = null;
    	String[] rez = new String[MAX_SIZE];
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "SELECT * FROM Universitate";
        	PreparedStatement pstmt = con.prepareStatement(query);
        	rs = pstmt.executeQuery();
        	
        	int k = 0;
        	while(rs.next())
        	{
        		rez[k] = rs.getString("denumire");
        		k++;
        	}
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return rez;
	}
	public boolean delete(String _ID)
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	boolean action = false;
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "DELETE FROM Universitate WHERE ID= " + _ID + ";";		
        	PreparedStatement pstmt = con.prepareStatement(query);
        	action = (pstmt.executeUpdate() > 0);
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return action;
	}
	public Universitate fs(String denumire)
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	ResultSet rs = null;
    	Universitate u = new Universitate();
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "SELECT * FROM Universitate WHERE denumire= \'" + denumire + "\';";	
        	PreparedStatement pstmt = con.prepareStatement(query);
        	pstmt = con.prepareStatement(query);
        	rs = pstmt.executeQuery();
        	if (rs.next()) {
        		 u = new Universitate(rs.getString("ID"),
        				rs.getString("denumire"),rs.getString("adresa"),rs.getString("tel"));
        	}
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return u;
	}
	public String cauta(String nume)
	{
		String path = new java.io.File("Universitati.accdb").getAbsolutePath();
    	String url = "jdbc:ucanaccess://"+path;
    	ResultSet rs = null;
    	String rez = "";
    	try {
    		Connection con = DriverManager.getConnection(url);
        	String query = "SELECT * FROM Universitate WHERE denumire LIKE " + "\'*" + nume + "*\';";
        	PreparedStatement pstmt = con.prepareStatement(query);
        	pstmt = con.prepareStatement(query);
        	rs = pstmt.executeQuery();
        	while (rs.next()) {
        	rez += rs.getString("ID");
        	rez += " ";
        	rez += rs.getString("denumire");
        	rez += " ";
        	rez += rs.getString("tel");	
        	}
        	con.close();
        	}catch(SQLException e) {
        		e.printStackTrace();
        	}
    	return rez;
	}
	public String getDenumire() {
		return denumire;
	}
	private void setDenumire(String denumire) {
		this.denumire = denumire;
	}
	public String getAdresa() {
		return adresa;
	}
	 void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getTel() {
		return tel;
	}
	void setTel(String tel) {
		this.tel = tel;
	}
	String getID() {
		return ID;
	}

}
