import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
@SuppressWarnings("serial")
public class GUICautaUniversitate extends JFrame {
	private JButton okbtn;
	private JTextField denumire;
	private JLabel rez; 
	private JLabel label;
	public GUICautaUniversitate()
	{
		JFrame.setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame("Cauta Universitate");
		rez = new JLabel("");
		frame.setLayout(new GridLayout(0,2));
		JPanel mainframe = new JPanel();
		mainframe.setLayout(new BorderLayout());
		okbtn = new JButton("OK");
		mainframe.add(okbtn,BorderLayout.EAST);
		denumire = new JTextField();
		label = new JLabel("Nume");	
		frame.add(label);
		frame.add(rez);
		frame.add(denumire);
		frame.add(mainframe);
		frame.setSize(600,100);
		Handler handle = new Handler();
		okbtn.addActionListener(handle);
		mainframe.setVisible(true);
		frame.setVisible(true);
	}
	private class Handler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
					if (e.getSource() == okbtn) {
						String ok = new Universitate().cauta(denumire.getText());
						if (ok.contentEquals("")) {
							JOptionPane.showMessageDialog(null, "Universitatea nu a fost gasita");
						}
						else {
							if (!denumire.getText().contentEquals(""))
							rez.setText(ok);
							else {
								JOptionPane.showMessageDialog(null, "Campul este gol");
					}
				}
			}
		}
	}	
}
