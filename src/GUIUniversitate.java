import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
@SuppressWarnings("serial")
public class GUIUniversitate extends JFrame{
	private JButton btn;
	private JButton showbtn;
	private JButton del;
	private JButton search;
	private JButton updatebtn;
public GUIUniversitate () {
	JFrame.setDefaultLookAndFeelDecorated(true);
	JFrame frame = new JFrame("Meniu Universitate");
	updatebtn = new JButton("Actualizare Universitate");
	search = new JButton("Cauta Universitate");
    btn = new JButton("Adauga Universitate");
    showbtn = new JButton("Arata Universitati");
    del = new JButton("Sterge Universitate");
	frame.setLayout(new GridLayout(5,1));
	frame.setLocationRelativeTo(null);
    frame.add(btn, BorderLayout.SOUTH);
    frame.add(showbtn,BorderLayout.SOUTH);
    frame.add(search,BorderLayout.SOUTH);
    frame.add(del,BorderLayout.SOUTH);
    frame.add(updatebtn,BorderLayout.SOUTH);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    Handle handler = new Handle();
    del.addActionListener(handler);
    showbtn.addActionListener(handler);
    btn.addActionListener(handler);
    search.addActionListener(handler);
    updatebtn.addActionListener(handler);
    
    frame.setVisible(true);
   }
private class Handle implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn)
		new GUIAdaugaUniversitate();
		else if (e.getSource() == showbtn)
			new GUIArataUniversitate();
		else if (e.getSource() == del){
			new GUIStergeUniversitate();
		}
		else if (e.getSource() == search){
			new GUICautaUniversitate();
		}
		else {
			new GUIModificaUniversitate();
		}
	}
}
}
