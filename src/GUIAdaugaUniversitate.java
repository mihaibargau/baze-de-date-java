import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

@SuppressWarnings("serial")
public class GUIAdaugaUniversitate extends JFrame {
	private JLabel l_denumire;
	private JLabel l_tel;
	private JLabel l_adresa;
	private JTextField denumire;
	private JTextField tel;
	private JTextField adresa;
	private JButton adaugabtn;
	public GUIAdaugaUniversitate() {
		setDefaultLookAndFeelDecorated(true);
		JFrame frame = new JFrame();
		adaugabtn = new JButton("Adauga Universitate");
		l_denumire = new JLabel("Denumire");
		denumire = new JTextField();
		l_tel = new JLabel("Telefon");
		tel = new JTextField();
		l_adresa = new JLabel("Adesa");
		adresa = new JTextField();
		frame.add(l_denumire);
		frame.add(denumire);
		frame.add(l_tel);
		frame.add(tel);
		frame.add(l_denumire);
		frame.add(denumire);
		frame.add(l_adresa);
		frame.add(adresa);
		frame.pack();
		frame.setLayout(new GridLayout(0,2));
		JPanel mainframe = new JPanel();
		mainframe.setLayout(new BorderLayout());
		mainframe.add(adaugabtn, BorderLayout.SOUTH);
		frame.add(mainframe);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setTitle("Adauga Universitate");
		Handle handle = new Handle();
		adaugabtn.addActionListener(handle);
		frame.setVisible(true);
	}
	private class Handle implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			if (!(denumire.getText().equals("") || adresa.getText().equals("") || tel.getText().equals(""))) {
			if(new Universitate(denumire.getText(),adresa.getText(),tel.getText()).save()) {
				JOptionPane.showMessageDialog(null, "Universitate adaugata");
			}
			else {
				JOptionPane.showMessageDialog(null, "Eroare");
			}
		}
			else {
				JOptionPane.showMessageDialog(null, "Campul este gol");
			}
		}
		
	}

}
